## Reviewer checklist

<!-- Please don't change or remove this checklist. It will be used by the
  -- reviewer to make sure to not forget important things.
  -- Reviewer: if you think one of the item isn’t applicable to this PR,
  -- please check it anyway.
  -->

- [ ] Code is manually tested
- [ ] Pull/merge request has been reviewed and approved
