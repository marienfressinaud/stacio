# Stacio

Stacio hosts your community and helps to manage your projects.

It's still under heavy development and features may appear or disappear at any moment.

It is licensed under [GNU Affero General Public License v3.0 or later](/LICENSE.txt).
