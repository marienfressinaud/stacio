<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\SetList;

return RectorConfig::configure()
    ->withPaths([
        __DIR__ . '/configuration',
        __DIR__ . '/public',
        __DIR__ . '/src',
    ])
    ->withSets([
        SetList::PHP_82,
        SetList::PHP_83,
        SetList::TYPE_DECLARATION,
    ]);

