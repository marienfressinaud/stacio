.DEFAULT_GOAL := help

USER = $(shell id -u):$(shell id -g)

DOCKER_COMPOSE = docker compose -f docker/development/docker-compose.yml

ifdef NODOCKER
	PHP = php
	COMPOSER = composer
	CLI = php cli
	NPM = npm
else
	PHP = ./docker/bin/php
	COMPOSER = ./docker/bin/composer
	CLI = ./docker/bin/cli
	NPM = ./docker/bin/npm
endif

.PHONY: docker-start
docker-start: PORT ?= 8000
docker-start: ## Start a development server (can take a PORT argument)
	@echo "Running webserver on http://localhost:$(PORT)"
	$(DOCKER_COMPOSE) up

.PHONY: docker-build
docker-build: ## Rebuild the Docker containers
	$(DOCKER_COMPOSE) build --pull

.PHONY: docker-pull
docker-pull: ## Pull the Docker images from the Docker Hub
	$(DOCKER_COMPOSE) pull --ignore-buildable

.PHONY: docker-clean
docker-clean: ## Clean the Docker stuff
	$(DOCKER_COMPOSE) down -v

.PHONY: db-migrate
db-migrate: .env ## Setup the application system
	$(CLI) migrations setup --seed

.PHONY: db-rollback
db-rollback: ## Reverse the last migration (can take a STEPS argument)
ifdef STEPS
	$(CLI) migrations rollback --steps=$(STEPS)
else
	$(CLI) migrations rollback
endif

.PHONY: db-reset
db-reset: ## Reset the database (take a FORCE argument)
ifndef FORCE
	$(error Please run the operation with FORCE=true)
endif
	$(DOCKER_COMPOSE) stop job_worker
	$(CLI) migrations reset --force --seed
	$(DOCKER_COMPOSE) start job_worker

.PHONY: install
install: INSTALLER ?= all
install: ## Install the dependencies (can take an INSTALLER argument)
ifeq ($(INSTALLER), $(filter $(INSTALLER), all composer))
	$(COMPOSER) install
endif
ifeq ($(INSTALLER), $(filter $(INSTALLER), all npm))
	$(NPM) install
endif

.PHONY: lint
lint: LINTER ?= all
lint: ## Run the linters (can take a LINTER argument)
ifeq ($(LINTER),$(filter $(LINTER), all phpstan))
	$(PHP) ./vendor/bin/phpstan analyse --memory-limit 1G -c .phpstan.neon
endif
ifeq ($(LINTER), $(filter $(LINTER), all rector))
	$(PHP) vendor/bin/rector process --dry-run --config .rector.php
endif
ifeq ($(LINTER),$(filter $(LINTER), all phpcs))
	$(PHP) vendor/bin/phpcs
endif

.PHONY: lint-fix
lint-fix: LINTER ?= all
lint-fix: ## Fix the errors detected by the linters (can take a LINTER argument)
ifeq ($(LINTER), $(filter $(LINTER), all rector))
	$(PHP) vendor/bin/rector process --config .rector.php
endif
ifeq ($(LINTER), $(filter $(LINTER), all phpcs))
	$(PHP) vendor/bin/phpcbf
endif

.PHONY: help
help:
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.env:
	@cp env.sample .env
