<?php

namespace App\cli;

use App\models;
use Minz\Request;
use Minz\Response;

class Users
{
    /**
     * List the registered users.
     *
     * @response 200
     */
    public function index(Request $request): Response
    {
        $users = models\User::listAll();

        usort($users, function ($user1, $user2): int {
            return $user1->id <=> $user2->id;
        });

        $output = [];

        foreach ($users as $user) {
            $created_at = $user->created_at->format('Y-m-d');
            $output[] = "{$user->id} {$created_at} {$user->username}";
        }

        if (!$output) {
            $output[] = 'No users';
        }

        return Response::text(200, implode("\n", $output));
    }

    /**
     * Create a user.
     *
     * @request_param string username
     * @request_param string password
     *
     * @response 400 if one of the param is invalid
     * @response 200
     */
    public function create(Request $request): Response
    {
        $username = $request->param('username', '');
        $password = $request->param('password', '');

        if (models\User::existsBy(['username' => $username])) {
            return Response::text(400, "User creation failed: username {$username} already exists");
        }

        $user = new models\User();
        $user->username = $username;
        $user->setPassword($password);

        /** @var array<string, string> */
        $errors = $user->validate();
        if ($errors) {
            $errors = implode(' ', $errors);
            return Response::text(400, "User creation failed: {$errors}");
        }

        $user->save();

        return Response::text(200, "User {$user->username} (id {$user->id}) has been created.");
    }
}
