<?php

namespace App;

use Minz\Request;
use Minz\Response;

/**
 * @phpstan-import-type ResponseReturnable from Response
 */
class Application
{
    /**
     * @return ResponseReturnable
     */
    public function run(Request $request): mixed
    {
        setlocale(LC_ALL, 'fr_FR.UTF8');

        if ($request->method() === 'CLI') {
            $this->initCli($request);
        } else {
            $this->initApp($request);
        }

        $response = \Minz\Engine::run($request);

        if ($response instanceof Response) {
            $response->setContentSecurityPolicy('img-src', '*');
        }

        return $response;
    }

    private function initApp(Request $request): void
    {
        $router = Router::load();

        \Minz\Engine::init($router, [
            'start_session' => true,
            'not_found_view_pointer' => 'not_found.phtml',
            'internal_server_error_view_pointer' => 'internal_server_error.phtml',
            'controller_namespace' => '\\App\\controllers',
        ]);

        /** @var ?string */
        $session_token_id = $request->cookie('session_token');

        if ($session_token_id) {
            auth\CurrentUser::setSessionTokenId($session_token_id);
        }

        \Minz\Output\View::declareDefaultVariables([
            'environment' => \Minz\Configuration::$environment,
            'csrf_token' => \Minz\Csrf::generate(),
            'errors' => [],
            'error' => null,
            'preserve_scroll' => false,
            'brand' => 'Stacio',
            'current_user' => auth\CurrentUser::get(),
        ]);
    }

    private function initCli(Request $request): void
    {
        $router = Router::loadCli();

        \Minz\Engine::init($router, [
            'not_found_view_pointer' => 'cli/not_found.txt',
            'internal_server_error_view_pointer' => 'cli/internal_server_error.txt',
            'controller_namespace' => '\\App\\cli',
        ]);

        $bin = $request->param('bin');
        $bin = $bin === 'cli' ? 'php cli' : $bin;

        $current_command = $request->path();
        $current_command = trim(str_replace('/', ' ', $current_command));

        \Minz\Output\View::declareDefaultVariables([
            'environment' => \Minz\Configuration::$environment,
            'errors' => [],
            'error' => null,
            'bin' => $bin,
            'current_command' => $current_command,
        ]);
    }
}
