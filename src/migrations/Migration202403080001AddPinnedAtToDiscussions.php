<?php

namespace App\migrations;

class Migration202403080001AddPinnedAtToDiscussions
{
    public function migrate(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE discussions
            ADD COLUMN pinned_at TIMESTAMPTZ;
        SQL);

        return true;
    }

    public function rollback(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE discussions
            DROP COLUMN pinned_at;
        SQL);

        return true;
    }
}
