<?php

namespace App\migrations;

class Migration202311190001AddCreatedByIdToOrganisationsAndSpaces
{
    public function migrate(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE organisations
            ADD COLUMN created_by_id INTEGER
            REFERENCES users ON DELETE CASCADE ON UPDATE CASCADE;

            ALTER TABLE spaces
            ADD COLUMN created_by_id INTEGER
            REFERENCES users ON DELETE CASCADE ON UPDATE CASCADE;

            UPDATE organisations
            SET created_by_id = (
                SELECT id FROM users
                ORDER BY created_at
                LIMIT 1
            );

            UPDATE spaces
            SET created_by_id = (
                SELECT id FROM users
                ORDER BY created_at
                LIMIT 1
            );

            ALTER TABLE organisations
            ALTER COLUMN created_by_id SET NOT NULL;

            ALTER TABLE spaces
            ALTER COLUMN created_by_id SET NOT NULL;
        SQL);

        return true;
    }

    public function rollback(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE organisations
            DROP COLUMN created_by_id;

            ALTER TABLE spaces
            DROP COLUMN created_by_id;
        SQL);

        return true;
    }
}
