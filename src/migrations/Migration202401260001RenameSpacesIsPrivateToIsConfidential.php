<?php

namespace App\migrations;

class Migration202401260001RenameSpacesIsPrivateToIsConfidential
{
    public function migrate(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE spaces
            RENAME COLUMN is_private TO is_confidential;
        SQL);

        return true;
    }

    public function rollback(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE spaces
            RENAME COLUMN is_confidential TO is_private;
        SQL);

        return true;
    }
}
