<?php

namespace App\migrations;

class Migration202312060001CreateUsersInOrganisations
{
    public function migrate(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            CREATE TABLE users_in_organisations (
                id SERIAL PRIMARY KEY,
                created_at TIMESTAMPTZ NOT NULL,
                updated_at TIMESTAMPTZ NOT NULL,

                user_id INTEGER NOT NULL REFERENCES users ON DELETE CASCADE ON UPDATE CASCADE,
                organisation_id INTEGER NOT NULL REFERENCES organisations ON DELETE CASCADE ON UPDATE CASCADE
            );
        SQL);

        $now = \Minz\Time::now();

        $statement = $database->query(<<<'SQL'
            SELECT * FROM organisations
        SQL);

        $db_organisations = $statement->fetchAll();

        foreach ($db_organisations as $db_organisation) {
            $statement = $database->prepare(<<<'SQL'
                INSERT INTO users_in_organisations (
                    created_at,
                    updated_at,
                    user_id,
                    organisation_id
                ) VALUES (
                    :created_at,
                    :updated_at,
                    :user_id,
                    :organisation_id
                );
            SQL);

            $statement->execute([
                ':created_at' => $now->format(\Minz\Database\Column::DATETIME_FORMAT),
                ':updated_at' => $now->format(\Minz\Database\Column::DATETIME_FORMAT),
                ':user_id' => $db_organisation['created_by_id'],
                ':organisation_id' => $db_organisation['id'],
            ]);
        }

        return true;
    }

    public function rollback(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            DROP TABLE users_in_organisations;
        SQL);

        return true;
    }
}
