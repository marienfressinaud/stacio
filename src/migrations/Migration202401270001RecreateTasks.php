<?php

namespace App\migrations;

class Migration202401270001RecreateTasks
{
    public function migrate(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            DROP TABLE tasks;

            CREATE TABLE tasks (
                id BIGSERIAL PRIMARY KEY,
                created_at TIMESTAMPTZ NOT NULL,
                updated_at TIMESTAMPTZ NOT NULL,

                title TEXT NOT NULL,
                notes TEXT NOT NULL DEFAULT '',
                scheduled_at TIMESTAMPTZ,
                done_at TIMESTAMPTZ,
                position INT NOT NULL,

                created_by_id INTEGER NOT NULL REFERENCES users ON DELETE CASCADE ON UPDATE CASCADE,
                space_id BIGINT NOT NULL REFERENCES spaces ON DELETE CASCADE ON UPDATE CASCADE
            );
        SQL);

        return true;
    }

    public function rollback(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            DROP TABLE tasks;

            CREATE TABLE tasks (
                id BIGSERIAL PRIMARY KEY,
                created_at TIMESTAMPTZ NOT NULL,
                updated_at TIMESTAMPTZ NOT NULL,

                done_at TIMESTAMPTZ,
                position INT NOT NULL,

                created_by_id INTEGER NOT NULL REFERENCES users ON DELETE CASCADE ON UPDATE CASCADE,
                discussion_id BIGINT NOT NULL REFERENCES discussions ON DELETE CASCADE ON UPDATE CASCADE
            );
        SQL);

        return true;
    }
}
