<?php

namespace App\migrations;

class Migration202401180001AddIsPrivateToSpaces
{
    public function migrate(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE spaces
            ADD COLUMN is_private BOOLEAN NOT NULL DEFAULT false;
        SQL);

        return true;
    }

    public function rollback(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE spaces
            DROP COLUMN is_private;
        SQL);

        return true;
    }
}
