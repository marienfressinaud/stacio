<?php

namespace App\migrations;

use App\models;

class Migration202312070001AddInvitationTokenIdToOrganisations
{
    public function migrate(): bool
    {
        $database = \Minz\Database::get();

        $database->beginTransaction();

        $database->exec(<<<'SQL'
            ALTER TABLE organisations
            ADD COLUMN invitation_token_id TEXT UNIQUE REFERENCES tokens ON DELETE CASCADE ON UPDATE CASCADE
        SQL);

        $statement = $database->query(<<<'SQL'
            SELECT * FROM organisations
        SQL);

        $db_organisations = $statement->fetchAll();

        foreach ($db_organisations as $db_organisation) {
            $token = new models\Token(999, 'year', length: 25);
            $token->save();

            $statement = $database->prepare(<<<'SQL'
                UPDATE organisations
                SET invitation_token_id = :token_id
                WHERE id = :organisation_id
            SQL);
            $statement->execute([
                ':token_id' => $token->id,
                ':organisation_id' => $db_organisation['id'],
            ]);
        }

        $database->exec(<<<'SQL'
            ALTER TABLE organisations
            ALTER COLUMN invitation_token_id SET NOT NULL
        SQL);

        $database->commit();

        return true;
    }

    public function rollback(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE organisations
            DROP COLUMN invitation_token_id
        SQL);

        return true;
    }
}
