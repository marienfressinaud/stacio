<?php

namespace App\migrations;

class Migration202311210001AddArchivedAtToDiscussions
{
    public function migrate(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE discussions
            ADD COLUMN archived_at TIMESTAMPTZ;
        SQL);

        return true;
    }

    public function rollback(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE discussions
            DROP COLUMN archived_at;
        SQL);

        return true;
    }
}
