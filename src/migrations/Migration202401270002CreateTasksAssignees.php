<?php

namespace App\migrations;

class Migration202401270002CreateTasksAssignees
{
    public function migrate(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            CREATE TABLE tasks_assignees (
                id BIGSERIAL PRIMARY KEY,
                created_at TIMESTAMPTZ NOT NULL,
                updated_at TIMESTAMPTZ NOT NULL,

                assignee_id INTEGER NOT NULL REFERENCES users ON DELETE CASCADE ON UPDATE CASCADE,
                task_id INTEGER NOT NULL REFERENCES tasks ON DELETE CASCADE ON UPDATE CASCADE
            );
        SQL);

        return true;
    }

    public function rollback(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            DROP TABLE tasks_assignees;
        SQL);

        return true;
    }
}
