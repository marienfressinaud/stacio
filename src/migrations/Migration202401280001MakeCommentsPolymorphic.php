<?php

namespace App\migrations;

class Migration202401280001MakeCommentsPolymorphic
{
    public function migrate(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE comments
            ADD COLUMN reference_type TEXT,
            ADD COLUMN reference_id BIGINT;

            UPDATE comments
            SET reference_type = 'discussion',
                reference_id = discussion_id;

            ALTER TABLE comments
            ALTER COLUMN reference_type SET NOT NULL,
            ALTER COLUMN reference_id SET NOT NULL,
            DROP COLUMN discussion_id;

            CREATE INDEX idx_comments_reference_type_reference_id
            ON comments(reference_type, reference_id);
        SQL);

        return true;
    }

    public function rollback(): bool
    {
        $database = \Minz\Database::get();

        $database->exec(<<<'SQL'
            ALTER TABLE comments
            ADD COLUMN discussion_id BIGINT REFERENCES discussions ON DELETE CASCADE ON UPDATE CASCADE;

            UPDATE comments
            SET discussion_id = reference_id;

            DROP INDEX idx_comments_reference_type_reference_id;

            ALTER TABLE comments
            ALTER COLUMN discussion_id SET NOT NULL,
            DROP COLUMN reference_type,
            DROP COLUMN reference_id;
        SQL);

        return true;
    }
}
