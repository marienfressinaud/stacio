<?php

namespace App;

class Router extends \Minz\Router
{
    public static function load(): self
    {
        $router = new self();

        $router->addRoute('GET', '/', 'Home#show', 'home');

        $router->addRoute('GET', '/registration', 'Users#new', 'registration');
        $router->addRoute('POST', '/registration', 'Users#create', 'register');
        $router->addRoute('GET', '/login', 'Sessions#new', 'login');
        $router->addRoute('POST', '/login', 'Sessions#create', 'log in');
        $router->addRoute('POST', '/logout', 'Sessions#delete', 'log out');

        $router->addRoute('GET', '/orgs', 'Organisations#index', 'organisations');
        $router->addRoute('GET', '/orgs/new', 'Organisations#new', 'new organisation');
        $router->addRoute('POST', '/orgs/new', 'Organisations#create', 'create organisation');
        $router->addRoute('GET', '/orgs/join/:token', 'Organisations#join', 'join organisation');
        $router->addRoute('GET', '/orgs/:id', 'Organisations#show', 'organisation');
        $router->addRoute('GET', '/orgs/:id/edit', 'Organisations#edit', 'edit organisation');
        $router->addRoute('POST', '/orgs/:id/edit', 'Organisations#update', 'update organisation');
        $router->addRoute('GET', '/orgs/:id/deletion', 'Organisations#deletion', 'organisation deletion');
        $router->addRoute('POST', '/orgs/:id/deletion', 'Organisations#delete', 'delete organisation');

        $router->addRoute('GET', '/orgs/:id/users', 'Organisations/Users#index', 'users');

        $router->addRoute('GET', '/orgs/:id/spaces', 'Organisations/Spaces#index', 'spaces');
        $router->addRoute('GET', '/orgs/:id/spaces/new', 'Organisations/Spaces#new', 'new space');
        $router->addRoute('POST', '/orgs/:id/spaces/new', 'Organisations/Spaces#create', 'create space');

        $router->addRoute('GET', '/spaces/:id', 'Spaces#show', 'space');
        $router->addRoute('GET', '/spaces/:id/edit', 'Spaces#edit', 'edit space');
        $router->addRoute('POST', '/spaces/:id/edit', 'Spaces#update', 'update space');
        $router->addRoute('GET', '/spaces/:id/deletion', 'Spaces#deletion', 'space deletion');
        $router->addRoute('POST', '/spaces/:id/deletion', 'Spaces#delete', 'delete space');

        $router->addRoute('GET', '/spaces/:id/discussions', 'Spaces/Discussions#index', 'discussions');
        $router->addRoute('GET', '/spaces/:id/discussions/archives', 'Spaces/Discussions#archives', 'archives');
        $router->addRoute('GET', '/spaces/:id/discussions/new', 'Spaces/Discussions#new', 'new discussion');
        $router->addRoute('POST', '/spaces/:id/discussions/new', 'Spaces/Discussions#create', 'create discussion');

        $router->addRoute('GET', '/spaces/:id/tasks', 'Spaces/Tasks#index', 'tasks');
        $router->addRoute('GET', '/spaces/:id/tasks/new', 'Spaces/Tasks#new', 'new task');
        $router->addRoute('POST', '/spaces/:id/tasks/new', 'Spaces/Tasks#create', 'create task');

        $router->addRoute('GET', '/discussions/:id', 'Discussions#show', 'discussion');
        $router->addRoute('GET', '/discussions/:id/edit', 'Discussions#edit', 'edit discussion');
        $router->addRoute('POST', '/discussions/:id/edit', 'Discussions#update', 'update discussion');
        $router->addRoute('GET', '/discussions/:id/deletion', 'Discussions#deletion', 'discussion deletion');
        $router->addRoute('POST', '/discussions/:id/deletion', 'Discussions#delete', 'delete discussion');

        $router->addRoute('POST', '/discussions/:id/archive', 'Discussions/Archives#update', 'archive discussion');
        $router->addRoute('POST', '/discussions/:id/pin', 'Discussions/Pins#update', 'pin discussion');

        $router->addRoute('POST', '/comments/new', 'Comments#create', 'create comment');
        $router->addRoute('GET', '/comments/:id/edit', 'Comments#edit', 'edit comment');
        $router->addRoute('POST', '/comments/:id/edit', 'Comments#update', 'update comment');
        $router->addRoute('POST', '/comments/:id/deletion', 'Comments#delete', 'delete comment');

        $router->addRoute('GET', '/workplan', 'Workplans#show', 'workplan');

        $router->addRoute('GET', '/tasks/:id', 'Tasks#show', 'task');
        $router->addRoute('GET', '/tasks/:id/edit', 'Tasks#edit', 'edit task');
        $router->addRoute('POST', '/tasks/:id/edit', 'Tasks#update', 'update task');
        $router->addRoute('POST', '/tasks/:id/done', 'Tasks#updateDone', 'update task done');
        $router->addRoute('POST', '/tasks/:id/position', 'Tasks#updatePosition', 'update task position');
        $router->addRoute('POST', '/tasks/:id/deletion', 'Tasks#delete', 'delete task');

        return $router;
    }

    public static function loadCli(): self
    {
        $router = self::load();

        $router->addRoute('CLI', '/help', 'Help#show');

        $router->addRoute('CLI', '/jobs', 'Jobs#index');
        $router->addRoute('CLI', '/jobs/watch', 'Jobs#watch');
        $router->addRoute('CLI', '/jobs/run', 'Jobs#run');
        $router->addRoute('CLI', '/jobs/show', 'Jobs#show');
        $router->addRoute('CLI', '/jobs/unfail', 'Jobs#unfail');
        $router->addRoute('CLI', '/jobs/unlock', 'Jobs#unlock');

        $router->addRoute('CLI', '/migrations', 'Migrations#index');
        $router->addRoute('CLI', '/migrations/setup', 'Migrations#setup');
        $router->addRoute('CLI', '/migrations/rollback', 'Migrations#rollback');
        $router->addRoute('CLI', '/migrations/create', 'Migrations#create');
        $router->addRoute('CLI', '/migrations/reset', 'Migrations#reset');

        $router->addRoute('CLI', '/users', 'Users#index');
        $router->addRoute('CLI', '/users/create', 'Users#create');

        return $router;
    }
}
