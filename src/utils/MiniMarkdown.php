<?php

namespace App\utils;

class MiniMarkdown extends \Parsedown
{
    public function __construct()
    {
        $this->setSafeMode(true)
             ->setBreaksEnabled(true);
    }
}
