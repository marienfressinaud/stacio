<?php

use App\auth;
use App\models;

function is_authorized(?models\User $user, string $action, object $subject): bool
{
    return auth\Access::isAuthorized($user, $action, $subject);
}
