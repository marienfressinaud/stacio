<?php

namespace App\utils;

class Sorter
{
    /**
     * Sort items by the given property.
     *
     * @template T of object
     *
     * @param T[] $items
     * @param 'asc'|'desc' $order
     *
     * @return T[]
     */
    public static function sortBy(array $items, string $property, string $order = 'asc'): array
    {
        usort($items, function ($item1, $item2) use ($property, $order): int {
            $property1 = $item1->$property;
            $property2 = $item2->$property;

            if ($property1 == $property2) {
                return 0;
            }

            if ($order === 'asc') {
                if ($property1 === null) {
                    return 1;
                }

                if ($property2 === null) {
                    return -1;
                }

                return $property1 <=> $property2;
            } else {
                if ($property1 === null) {
                    return -1;
                }

                if ($property2 === null) {
                    return 1;
                }

                return $property2 <=> $property1;
            }
        });

        return $items;
    }
}
