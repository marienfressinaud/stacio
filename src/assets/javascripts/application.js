import * as Turbo from '@hotwired/turbo';
import { Application } from '@hotwired/stimulus';

import AutosaveController from './controllers/autosave_controller.js';
import TextEditorController from './controllers/text_editor_controller.js';

const application = Application.start();
application.register('autosave', AutosaveController);
application.register('text-editor', TextEditorController);
