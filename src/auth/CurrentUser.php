<?php

namespace App\auth;

use App\models;

class CurrentUser
{
    private static ?models\User $instance = null;

    public static function get(): ?models\User
    {
        if (!self::sessionTokenExists()) {
            return null;
        }

        if (self::$instance !== null) {
            return self::$instance;
        }

        $session_token_id = self::sessionTokenId();

        if (!$session_token_id) {
            return null;
        }

        $user = models\User::findBySessionTokenId($session_token_id);

        if (!$user) {
            unset($_SESSION['session_token_id']);
            return null;
        }

        self::$instance = $user;
        return self::$instance;
    }

    public static function require(): models\User
    {
        $current_user = self::get();

        if (!$current_user) {
            throw new MissingCurrentUserError();
        }

        return $current_user;
    }

    public static function setSessionToken(models\Token $token): void
    {
        self::reset();

        if ($token->hasExpired()) {
            \Minz\Log::warning('Trying to setSessionToken an expired token.');
            return;
        }

        $_SESSION['session_token_id'] = $token->id;
    }

    public static function setSessionTokenId(string $token_id): void
    {
        $token = models\Token::find($token_id);
        if ($token) {
            self::setSessionToken($token);
        }
    }

    public static function sessionTokenId(): ?string
    {
        if (!self::sessionTokenExists()) {
            return null;
        }

        $session_token_id = $_SESSION['session_token_id'];

        if (!is_string($session_token_id)) {
            return null;
        }

        return $session_token_id;
    }

    public static function sessionTokenExists(): bool
    {
        return isset($_SESSION['session_token_id']);
    }

    public static function reset(): void
    {
        unset($_SESSION['session_token_id']);
        self::$instance = null;
    }
}
