<?php

namespace App\auth;

use App\models;

class Access
{
    /** @var string[] */
    public static array $supported_actions = [
        'read:discussion',
        'read:organisation.users',
        'read:space',
        'read:task',
        'write:comment',
        'write:discussion',
        'write:organisation',
        'write:organisation.spaces',
        'write:organisation.users',
        'write:space',
        'write:space.comments',
        'write:space.discussions',
        'write:space.tasks',
        'write:task',
    ];

    public static function isAuthorized(?models\User $user, string $action, object $subject): bool
    {
        if (!in_array($action, self::$supported_actions)) {
            throw new \LogicException("{$action} is not a supported action.");
        }

        if ($subject instanceof models\Comment) {
            return self::isAuthorizedForComment($user, $action, $subject);
        } elseif ($subject instanceof models\Discussion) {
            return self::isAuthorizedForDiscussion($user, $action, $subject);
        } elseif ($subject instanceof models\Organisation) {
            return self::isAuthorizedForOrganisation($user, $action, $subject);
        } elseif ($subject instanceof models\Space) {
            return self::isAuthorizedForSpace($user, $action, $subject);
        } elseif ($subject instanceof models\Task) {
            return self::isAuthorizedForTask($user, $action, $subject);
        } else {
            $class_name = $subject::class;
            throw new \LogicException("Unexpected object of type {$class_name}.");
        }
    }

    public static function isAuthorizedForComment(
        ?models\User $user,
        string $action,
        models\Comment $comment
    ): bool {
        if ($action === 'write:comment') {
            return (
                $user && (
                    $user->id === $comment->created_by_id ||
                    $user->id === $comment->organisation()->created_by_id
                )
            );
        } else {
            throw new \LogicException("Unexpected action {$action} for object of type Comment.");
        }
    }

    public static function isAuthorizedForDiscussion(
        ?models\User $user,
        string $action,
        models\Discussion $discussion
    ): bool {
        if ($action === 'write:discussion') {
            return (
                $user && (
                    $user->id === $discussion->created_by_id ||
                    $user->id === $discussion->organisation()->created_by_id
                )
            );
        } elseif ($action === 'read:discussion') {
            $space = $discussion->space();
            return (
                !$space->is_confidential ||
                (
                    $user &&
                    models\UserInOrganisation::isUserInOrganisation($user, $space->organisation())
                )
            );
        } else {
            throw new \LogicException("Unexpected action {$action} for object of type Discussion.");
        }
    }

    public static function isAuthorizedForOrganisation(
        ?models\User $user,
        string $action,
        models\Organisation $organisation
    ): bool {
        if ($action === 'write:organisation') {
            return (
                $user &&
                $user->id === $organisation->created_by_id
            );
        } elseif (
            $action === 'read:organisation.users' ||
            $action === 'write:organisation.spaces' ||
            $action === 'write:organisation.users'
        ) {
            return $user && models\UserInOrganisation::isUserInOrganisation($user, $organisation);
        } else {
            throw new \LogicException("Unexpected action {$action} for object of type Organisation.");
        }
    }

    public static function isAuthorizedForSpace(
        ?models\User $user,
        string $action,
        models\Space $space
    ): bool {
        if ($action === 'write:space') {
            return (
                $user && (
                    $user->id === $space->created_by_id ||
                    $user->id === $space->organisation()->created_by_id
                )
            );
        } elseif (
            $action === 'write:space.comments' ||
            $action === 'write:space.discussions' ||
            $action === 'write:space.tasks'
        ) {
            return $user && models\UserInOrganisation::isUserInOrganisation($user, $space->organisation());
        } elseif ($action === 'read:space') {
            return (
                !$space->is_confidential ||
                (
                    $user &&
                    models\UserInOrganisation::isUserInOrganisation($user, $space->organisation())
                )
            );
        } else {
            throw new \LogicException("Unexpected action {$action} for object of type Space.");
        }
    }

    public static function isAuthorizedForTask(
        ?models\User $user,
        string $action,
        models\Task $task
    ): bool {
        if ($action === 'write:task') {
            return $user && models\UserInOrganisation::isUserInOrganisation($user, $task->organisation());
        } elseif ($action === 'read:task') {
            $space = $task->space();
            return (
                !$space->is_confidential ||
                (
                    $user &&
                    models\UserInOrganisation::isUserInOrganisation($user, $space->organisation())
                )
            );
        } else {
            throw new \LogicException("Unexpected action {$action} for object of type Task.");
        }
    }
}
