<?php

namespace App\auth;

class MissingCurrentUserError extends \RuntimeException
{
}
