<?php

namespace App\models;

use App\utils;
use Minz\Database;
use Minz\Validable;

#[Database\Table(name: 'discussions')]
class Discussion
{
    use Commentable;
    use Database\Recordable;
    use Requirable;
    use Validable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTimeImmutable $created_at;

    #[Database\Column]
    public \DateTimeImmutable $updated_at;

    #[Database\Column]
    #[Validable\Presence(
        message: 'Le titre est requis.',
    )]
    #[Validable\Length(
        max: 255,
        message: 'Le titre doit faire {max} caractères maximum.',
    )]
    public string $title = '';

    #[Database\Column]
    #[Validable\Presence(
        message: 'La description est requise.',
    )]
    public string $description = '';

    #[Database\Column]
    public int $created_by_id;

    #[Database\Column]
    public int $space_id;

    #[Database\Column]
    public ?\DateTimeImmutable $archived_at = null;

    #[Database\Column]
    public ?\DateTimeImmutable $pinned_at = null;

    public function __construct(Space $space, User $created_by)
    {
        $this->created_by_id = $created_by->id;
        $this->space_id = $space->id;
    }

    public function createdBy(): User
    {
        $user = User::find($this->created_by_id);
        assert($user !== null);
        return $user;
    }

    public function space(): Space
    {
        $space = Space::find($this->space_id);
        assert($space !== null);
        return $space;
    }

    public function organisation(): Organisation
    {
        $organisation = Organisation::findByDiscussion($this);
        assert($organisation !== null);
        return $organisation;
    }

    public function descriptionAsHtml(): string
    {
        $markdown = new utils\MiniMarkdown();
        return $markdown->text($this->description);
    }

    public function isPinned(): bool
    {
        return $this->pinned_at !== null;
    }

    /**
     * @return Discussion[]
     */
    public static function listArchivesFromSpace(Space $space): array
    {
        $table_name = self::tableName();

        $sql = <<<SQL
            SELECT * FROM {$table_name}
            WHERE space_id = :space_id
            AND archived_at IS NOT NULL
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':space_id' => $space->id,
        ]);
        return self::fromDatabaseRows($statement->fetchAll());
    }

    public static function countArchivesFromSpace(Space $space): int
    {
        $table_name = self::tableName();

        $sql = <<<SQL
            SELECT COUNT(*) FROM {$table_name}
            WHERE space_id = :space_id
            AND archived_at IS NOT NULL
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':space_id' => $space->id,
        ]);
        return intval($statement->fetchColumn());
    }

    /**
     * @return Discussion[]
     */
    public static function listFromSpace(Space $space): array
    {
        return self::listBy([
            'space_id' => $space->id,
            'archived_at' => null,
        ]);
    }
}
