<?php

namespace App\models;

use Minz\Database;

#[Database\Table(name: 'tokens')]
class Token
{
    use Database\Recordable;

    #[Database\Column]
    public string $id;

    #[Database\Column]
    public \DateTimeImmutable $created_at;

    #[Database\Column]
    public \DateTimeImmutable $updated_at;

    #[Database\Column]
    public \DateTimeImmutable $expired_at;

    public function __construct(int $number, string $duration, int $length = 64)
    {
        $this->id = \Minz\Random::hex($length);
        $this->expired_at = \Minz\Time::fromNow($number, $duration);
    }

    public function hasExpired(): bool
    {
        return $this->expiresIn(0, 'seconds');
    }

    public function expiresIn(int $number, string $unit): bool
    {
        return \Minz\Time::fromNow($number, $unit) >= $this->expired_at;
    }
}
