<?php

namespace App\models;

use Minz\Database;
use Minz\Validable;

#[Database\Table(name: 'spaces')]
class Space
{
    use Database\Recordable;
    use Requirable;
    use Validable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTimeImmutable $created_at;

    #[Database\Column]
    public \DateTimeImmutable $updated_at;

    #[Database\Column]
    #[Validable\Presence(
        message: 'Le nom est requis.',
    )]
    #[Validable\Length(
        max: 200,
        message: 'Le nom doit faire {max} caractères maximum.',
    )]
    public string $name = '';

    #[Database\Column]
    public string $description = '';

    #[Database\Column]
    public bool $is_confidential = false;

    #[Database\Column]
    public int $created_by_id;

    #[Database\Column]
    public int $organisation_id;

    public function __construct(Organisation $organisation, User $created_by)
    {
        $this->organisation_id = $organisation->id;
        $this->created_by_id = $created_by->id;
    }

    public function organisation(): Organisation
    {
        $organisation = Organisation::find($this->organisation_id);
        assert($organisation !== null);
        return $organisation;
    }

    /**
     * @return Discussion[]
     */
    public function discussions(): array
    {
        return Discussion::listFromSpace($this);
    }

    /**
     * @return Discussion[]
     */
    public function archives(): array
    {
        return Discussion::listArchivesFromSpace($this);
    }

    /**
     * @return Task[]
     */
    public function activeTasks(): array
    {
        return Task::listActiveBySpace($this);
    }

    /**
     * @return Task[]
     */
    public function doneTasks(): array
    {
        return Task::listDoneBySpace($this);
    }
}
