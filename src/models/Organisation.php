<?php

namespace App\models;

use Minz\Database;
use Minz\Validable;

#[Database\Table(name: 'organisations')]
class Organisation
{
    use Database\Recordable;
    use Requirable;
    use Validable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTimeImmutable $created_at;

    #[Database\Column]
    public \DateTimeImmutable $updated_at;

    #[Database\Column]
    #[Validable\Presence(
        message: 'Le nom est requis.',
    )]
    #[Validable\Length(
        max: 25,
        message: 'Le nom doit faire {max} caractères maximum.',
    )]
    public string $name = '';

    #[Database\Column]
    public int $created_by_id;

    #[Database\Column]
    public string $invitation_token_id;

    public function __construct(User $created_by)
    {
        $this->created_by_id = $created_by->id;
    }

    /**
     * @return Space[]
     */
    public function spaces(bool $confidential = true): array
    {
        $criteria = ['organisation_id' => $this->id];
        if (!$confidential) {
            $criteria['is_confidential'] = false;
        }

        return Space::listBy($criteria, 'created_at ASC');
    }

    /**
     * @return User[]
     */
    public function users(): array
    {
        return User::listInOrganisation($this);
    }

    public function countUsers(): int
    {
        return UserInOrganisation::countBy([
            'organisation_id' => $this->id,
        ]);
    }

    public static function findByDiscussion(Discussion $discussion): ?Organisation
    {
        $sql = <<<'SQL'
            SELECT o.* FROM organisations o, spaces s
            WHERE o.id = s.organisation_id
            AND s.id = :space_id
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':space_id' => $discussion->space_id,
        ]);

        $result = $statement->fetch();
        if (is_array($result)) {
            return self::fromDatabaseRow($result);
        } else {
            return null;
        }
    }

    public static function findByTask(Task $task): ?Organisation
    {
        $sql = <<<'SQL'
            SELECT o.* FROM organisations o, spaces s
            WHERE o.id = s.organisation_id
            AND s.id = :space_id
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':space_id' => $task->space_id,
        ]);

        $result = $statement->fetch();
        if (is_array($result)) {
            return self::fromDatabaseRow($result);
        } else {
            return null;
        }
    }

    public static function findByInvitationTokenId(string $invitation_token_id): ?Organisation
    {
        $sql = <<<'SQL'
            SELECT o.* FROM organisations o, tokens t
            WHERE o.invitation_token_id = t.id
            AND t.id = :invitation_token_id
            AND t.expired_at > :now
        SQL;

        $now = \Minz\Time::now();

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':invitation_token_id' => $invitation_token_id,
            ':now' => $now->format(Database\Column::DATETIME_FORMAT),
        ]);

        $result = $statement->fetch();
        if (is_array($result)) {
            return self::fromDatabaseRow($result);
        } else {
            return null;
        }
    }
}
