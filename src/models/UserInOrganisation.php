<?php

namespace App\models;

use Minz\Database;

#[Database\Table(name: 'users_in_organisations')]
class UserInOrganisation
{
    use Database\Recordable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTimeImmutable $created_at;

    #[Database\Column]
    public \DateTimeImmutable $updated_at;

    #[Database\Column]
    public int $user_id;

    #[Database\Column]
    public int $organisation_id;

    public function __construct(User $user, Organisation $organisation)
    {
        $this->user_id = $user->id;
        $this->organisation_id = $organisation->id;
    }

    public static function isUserInOrganisation(User $user, Organisation $organisation): bool
    {
        return self::existsBy([
            'user_id' => $user->id,
            'organisation_id' => $organisation->id,
        ]);
    }
}
