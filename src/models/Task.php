<?php

namespace App\models;

use App\utils;
use Minz\Database;
use Minz\Validable;

#[Database\Table(name: 'tasks')]
class Task
{
    use Commentable;
    use Database\Recordable;
    use Requirable;
    use Validable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTimeImmutable $created_at;

    #[Database\Column]
    public \DateTimeImmutable $updated_at;

    #[Database\Column]
    #[Validable\Presence(
        message: 'Le titre est requis.',
    )]
    #[Validable\Length(
        max: 255,
        message: 'Le titre doit faire {max} caractères maximum.',
    )]
    public string $title = '';

    #[Database\Column]
    public string $notes = '';

    #[Database\Column]
    public ?\DateTimeImmutable $scheduled_at = null;

    #[Database\Column]
    public ?\DateTimeImmutable $done_at = null;

    #[Database\Column]
    public int $position = 0;

    #[Database\Column]
    public int $created_by_id;

    #[Database\Column]
    public int $space_id;

    public function __construct(Space $space, User $createdBy)
    {
        $this->space_id = $space->id;
        $this->created_by_id = $createdBy->id;
    }

    public function createdBy(): User
    {
        $user = User::find($this->created_by_id);

        assert($user !== null);

        return $user;
    }

    public function space(): Space
    {
        $space = Space::find($this->space_id);
        assert($space !== null);
        return $space;
    }

    public function organisation(): Organisation
    {
        $organisation = Organisation::findByTask($this);

        assert($organisation !== null);

        return $organisation;
    }

    public function notesAsHtml(): string
    {
        $markdown = new utils\MiniMarkdown();
        return $markdown->text($this->notes);
    }

    /**
     * @return User[]
     */
    public function assignees(): array
    {
        return User::listForTask($this);
    }

    public function insertLast(): void
    {
        $this->position = $this->maxPosition();
        $this->save();
    }

    public function done(): void
    {
        $this->done_at = \Minz\Time::now();
        $this->moveOut();
    }

    public function undone(): void
    {
        $this->done_at = null;
        $this->insertLast();
    }

    public function moveAtPosition(int $new_position): void
    {
        $old_position = $this->position;

        if ($old_position === $new_position) {
            return;
        }

        if ($new_position < 1) {
            $new_position = 1;
        }

        $max_position = $this->maxPosition();
        if ($new_position > $max_position) {
            $new_position = $max_position;
        }

        $database = Database::get();
        $database->beginTransaction();

        if ($new_position > $old_position) {
            $sql = <<<'SQL'
                UPDATE tasks
                SET position = position - 1
                WHERE space_id = :space_id
                AND position > :old_position
                AND position <= :new_position
            SQL;
        } else {
            $sql = <<<'SQL'
                UPDATE tasks
                SET position = position + 1
                WHERE space_id = :space_id
                AND position < :old_position
                AND position >= :new_position
            SQL;
        }

        $statement = $database->prepare($sql);
        $statement->execute([
            ':space_id' => $this->space_id,
            ':old_position' => $old_position,
            ':new_position' => $new_position,
        ]);

        $this->position = $new_position;
        $this->save();

        $database->commit();
    }

    public function moveOut(): void
    {
        $old_position = $this->position;

        if ($old_position === -1) {
            return;
        }

        $database = Database::get();
        $database->beginTransaction();

        $sql = <<<'SQL'
            UPDATE tasks
            SET position = position - 1
            WHERE space_id = :space_id
            AND position > :old_position
        SQL;

        $statement = $database->prepare($sql);
        $statement->execute([
            ':space_id' => $this->space_id,
            ':old_position' => $old_position,
        ]);

        $this->position = -1;
        $this->save();

        $database->commit();
    }

    public function maxPosition(): int
    {
        $sql = <<<'SQL'
            SELECT MAX(position) FROM tasks
            WHERE space_id = :space_id
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':space_id' => $this->space_id,
        ]);

        /** @var int */
        $position = $statement->fetchColumn();

        if ($position < 0) {
            $position = 0;
        }

        return $position + 1;
    }

    /**
     * @return Task[]
     */
    public static function listActiveBySpace(Space $space): array
    {
        return Task::listBy([
            'space_id' => $space->id,
            'done_at' => null,
        ]);
    }

    /**
     * @return Task[]
     */
    public static function listDoneBySpace(Space $space): array
    {
        $sql = <<<SQL
            SELECT * FROM tasks
            WHERE space_id = :space_id
            AND done_at IS NOT NULL
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':space_id' => $space->id,
        ]);
        return self::fromDatabaseRows($statement->fetchAll());
    }

    /**
     * @return Task[]
     */
    public static function listAssignedToUser(User $user, bool $done): array
    {
        $sql = <<<SQL
            SELECT t.* FROM tasks t, tasks_assignees ta
            WHERE ta.task_id = t.id
            AND ta.assignee_id = :user_id
        SQL;

        if ($done) {
            $sql .= ' AND done_at IS NOT NULL';
        } else {
            $sql .= ' AND done_at IS NULL';
        }

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':user_id' => $user->id,
        ]);

        return self::fromDatabaseRows($statement->fetchAll());
    }
}
