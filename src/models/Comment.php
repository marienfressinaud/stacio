<?php

namespace App\models;

use App\utils;
use Minz\Database;
use Minz\Validable;

#[Database\Table(name: 'comments')]
class Comment
{
    use Database\Recordable;
    use Requirable;
    use Validable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTimeImmutable $created_at;

    #[Database\Column]
    public \DateTimeImmutable $updated_at;

    #[Database\Column]
    #[Validable\Presence(
        message: 'Le contenu est requis.',
    )]
    public string $content = '';

    #[Database\Column]
    public int $created_by_id;

    #[Database\Column]
    public string $reference_type = '';

    #[Database\Column]
    public int $reference_id = 0;

    public function __construct(User $created_by)
    {
        $this->created_by_id = $created_by->id;
    }

    public function createdBy(): User
    {
        $user = User::find($this->created_by_id);

        assert($user !== null);

        return $user;
    }

    public function reference(): Discussion|Task
    {
        $reference = null;

        if ($this->reference_type === 'discussion') {
            $reference = Discussion::find($this->reference_id);
        } elseif ($this->reference_type === 'task') {
            $reference = Task::find($this->reference_id);
        }

        if ($reference === null) {
            throw new \Exception('Unsupported reference type: ' . $this->reference_type);
        }

        return $reference;
    }

    public function referenceUrl(): string
    {
        if ($this->reference_type === 'discussion') {
            return \Minz\Url::for('discussion', [
                'id' => $this->reference_id,
            ]);
        } elseif ($this->reference_type === 'task') {
            return \Minz\Url::for('task', [
                'id' => $this->reference_id,
            ]);
        }

        throw new \Exception('Unsupported reference type: ' . $this->reference_type);
    }

    public function organisation(): Organisation
    {
        return $this->reference()->organisation();
    }

    public function contentAsHtml(): string
    {
        $markdown = new utils\MiniMarkdown();
        return $markdown->text($this->content);
    }
}
