<?php

namespace App\models;

trait Commentable
{
    public function commentableType(): string
    {
        $type = self::class;

        if (str_starts_with($type, 'App\\models\\')) {
            $type = substr($type, strlen('App\\models\\'));
        }

        $type = strtolower($type);

        return $type;
    }

    /**
     * @return Comment[]
     */
    public function comments(): array
    {
        return Comment::listBy([
            'reference_type' => $this->commentableType(),
            'reference_id' => $this->id,
        ], 'created_at ASC');
    }

    public function countComments(): int
    {
        return Comment::countBy([
            'reference_type' => $this->commentableType(),
            'reference_id' => $this->id,
        ]);
    }
}
