<?php

namespace App\models;

use Minz\Database;
use Minz\Validable;

#[Database\Table(name: 'users')]
class User
{
    use Database\Recordable;
    use Validable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTimeImmutable $created_at;

    #[Database\Column]
    public \DateTimeImmutable $updated_at;

    #[Database\Column]
    #[Validable\Presence(
        message: 'Le nom d’utilisateur est requis.',
    )]
    #[Validable\Length(
        max: 50,
        message: 'Le nom d’utilisateur doit faire {max} caractères maximum.',
    )]
    #[Validable\Format(
        pattern: '/^[\w]+$/',
        message: 'Le nom d’utilisateur ne peut contenir que des lettres et des chiffres.',
    )]
    public string $username = '';

    #[Database\Column]
    #[Validable\Presence(
        message: 'Le mot de passe est requis',
    )]
    public string $password_hash = '';

    #[Database\Column]
    public string $avatar_filename = '';

    public function setPassword(string $password): void
    {
        $this->password_hash = self::passwordHash($password);
    }

    public function verifyPassword(string $password): bool
    {
        return password_verify($password, $this->password_hash);
    }

    /**
     * @return Task[]
     */
    public function tasks(): array
    {
        return Task::listAssignedToUser($this, done: false);
    }

    public static function passwordHash(string $password): string
    {
        return $password ? password_hash($password, PASSWORD_BCRYPT) : '';
    }

    public static function findBySessionTokenId(string $session_token_id): ?User
    {
        $sql = <<<SQL
            SELECT u.* FROM users u, tokens t, sessions s

            WHERE u.id = s.user_id
            AND t.id = s.token_id
            AND t.id = :token_id
            AND t.expired_at > :expired_at
        SQL;

        $now = \Minz\Time::now()->format(Database\Column::DATETIME_FORMAT);

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':token_id' => $session_token_id,
            ':expired_at' => $now,
        ]);

        $result = $statement->fetch();
        if (is_array($result)) {
            return self::fromDatabaseRow($result);
        } else {
            return null;
        }
    }

    /**
     * @return User[]
     */
    public static function listInOrganisation(Organisation $organisation): array
    {
        $sql = <<<SQL
            SELECT u.* FROM users u, users_in_organisations uo

            WHERE u.id = uo.user_id
            AND uo.organisation_id = :organisation_id
            ORDER BY u.username
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':organisation_id' => $organisation->id,
        ]);

        return self::fromDatabaseRows($statement->fetchAll());
    }

    /**
     * @return User[]
     */
    public static function listForTask(Task $task): array
    {
        $sql = <<<'SQL'
            SELECT u.* FROM users u, tasks_assignees ta
            WHERE ta.assignee_id = u.id
            AND ta.task_id = :task_id
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':task_id' => $task->id,
        ]);

        return self::fromDatabaseRows($statement->fetchAll());
    }
}
