<?php

namespace App\models;

use Minz\Database;

#[Database\Table(name: 'sessions')]
class Session
{
    use Database\Recordable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTimeImmutable $created_at;

    #[Database\Column]
    public \DateTimeImmutable $updated_at;

    #[Database\Column]
    public int $user_id;

    #[Database\Column]
    public string $token_id;

    public function __construct(User $user, Token $token)
    {
        $this->user_id = $user->id;
        $this->token_id = $token->id;
    }
}
