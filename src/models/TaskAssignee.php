<?php

namespace App\models;

use App\utils;
use Minz\Database;

#[Database\Table(name: 'tasks_assignees')]
class TaskAssignee
{
    use Database\Recordable;
    use Requirable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTimeImmutable $created_at;

    #[Database\Column]
    public \DateTimeImmutable $updated_at;

    #[Database\Column]
    public int $assignee_id;

    #[Database\Column]
    public int $task_id;

    public function __construct(User $user, Task $task)
    {
        $this->assignee_id = $user->id;
        $this->task_id = $task->id;
    }

    /**
     * @param User[] $assignees
     */
    public static function set(Task $task, array $assignees): void
    {
        self::deleteBy([
            'task_id' => $task->id,
        ]);

        foreach ($assignees as $assignee) {
            $task_assignee = new self($assignee, $task);
            $task_assignee->save();
        }
    }
}
