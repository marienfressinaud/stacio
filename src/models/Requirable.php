<?php

namespace App\models;

trait Requirable
{
    public static function require(
        \Minz\Request $request,
        string $parameter = 'id'
    ): self {
        $class = self::class;
        /** @phpstan-ignore-next-line */
        if (!is_callable([$class, 'find'])) {
            throw new \LogicException("{$class} is not a \\Minz\\Database\\Recordable resource");
        }

        $id = $request->param($parameter);

        if ($id === null) {
            throw new MissingResourceError();
        }

        $resource = self::find($id);

        if ($resource === null) {
            throw new MissingResourceError();
        }

        return $resource;
    }
}
