<?php

namespace App\forms;

use Minz\Form;

/**
 * @extends BaseForm<\App\models\Task>
 */
class NewTask extends BaseForm
{
    #[Form\Field(trim: true)]
    public string $title = '';
}
