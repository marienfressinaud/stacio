<?php

namespace App\forms;

use App\models;
use Minz\Form;

/**
 * @extends BaseForm<\stdClass>
 */
class Login extends BaseForm
{
    #[Form\Field(trim: true)]
    public string $username = '';

    #[Form\Field]
    public string $password = '';

    #[Form\Check]
    public function checkCredentials(): void
    {
        try {
            $user = $this->getUser();
        } catch (\RuntimeException $e) {
            $this->addError('username', 'Il n’existe aucun compte correspondant à ce pseudonyme.');
            return;
        }

        if (!$user->verifyPassword($this->password)) {
            $this->addError('password', 'Le mot de passe est incorrect, veuillez réessayer.');
        }
    }

    public function getUser(): models\User
    {
        $user = models\User::findBy(['username' => $this->username]);

        if (!$user) {
            throw new \RuntimeException("Unknown user {$this->username}");
        }

        return $user;
    }
}
