<?php

namespace App\forms;

use App\auth;
use App\models;
use Minz\Form;

/**
 * @extends BaseForm<models\Task>
 */
class Task extends BaseForm
{
    #[Form\Field(trim: true)]
    public string $title = '';

    #[Form\Field(trim: true)]
    public string $notes = '';

    #[Form\Field(format: 'Y-m-d')]
    public ?\DateTimeImmutable $scheduled_at = null;

    /** @var string[] */
    #[Form\Field(bind_model: false)]
    public array $assignees_ids = [];

    /**
     * @param array<string, mixed> $default_values
     */
    public function __construct(array $default_values = [], ?models\Task $model = null)
    {
        if ($model) {
            $default_values['assignees_ids'] = array_column($model->assignees(), 'id');
        }

        parent::__construct($default_values, $model);
    }

    /**
     * @return models\User[]
     */
    public function getAssignees(): array
    {
        if (!$this->model) {
            return [];
        }

        $assignees = [];
        foreach ($this->assignees_ids as $user_id) {
            $user = models\User::find($user_id);

            if ($user === null || !auth\Access::isAuthorized($user, 'write:task', $this->model)) {
                continue;
            }

            $assignees[] = $user;
        }

        return $assignees;
    }
}
