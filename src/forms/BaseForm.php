<?php

namespace App\forms;

use Minz\Form;

/**
 * @template T of object
 *
 * @phpstan-extends Form<T>
 */
class BaseForm extends Form
{
    use Form\Csrf;

    public function csrfErrorMessage(): string
    {
        return 'Le jeton de sécurité est invalide. Veuillez essayer de revalider le formulaire.';
    }
}
