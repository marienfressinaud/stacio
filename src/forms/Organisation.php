<?php

namespace App\forms;

use Minz\Form;

/**
 * @extends BaseForm<\App\models\Organisation>
 */
class Organisation extends BaseForm
{
    #[Form\Field(trim: true)]
    public string $name = '';
}
