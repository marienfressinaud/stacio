<?php

namespace App\forms;

use Minz\Form;

/**
 * @extends BaseForm<\App\models\Space>
 */
class Space extends BaseForm
{
    #[Form\Field(trim: true)]
    public string $name = '';

    #[Form\Field(trim: true)]
    public string $description = '';

    #[Form\Field]
    public bool $is_confidential = false;
}
