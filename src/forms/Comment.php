<?php

namespace App\forms;

use App\models;
use Minz\Form;

/**
 * @extends BaseForm<models\Comment>
 */
class Comment extends BaseForm
{
    #[Form\Field]
    public string $reference_type = '';

    #[Form\Field]
    public int $reference_id = 0;

    #[Form\Field(trim: true)]
    public string $content = '';

    /**
     * @param models\Discussion|models\Task $reference
     */
    public static function buildFromReference(mixed $reference): self
    {
        $default_values = [];
        $default_values['reference_type'] = $reference->commentableType();
        $default_values['reference_id'] = $reference->id;

        return new self($default_values);
    }
}
