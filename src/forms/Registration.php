<?php

namespace App\forms;

use App\models;
use Minz\Form;

/**
 * @extends BaseForm<models\User>
 */
class Registration extends BaseForm
{
    #[Form\Field(trim: true)]
    public string $username = '';

    #[Form\Field(bind_model: 'setPassword')]
    public string $password = '';

    #[Form\Field(bind_model: false)]
    public string $invitation_token_id = '';

    #[Form\Check]
    public function checkInvitationTokenExists(): void
    {
        $organisation = models\Organisation::findByInvitationTokenId($this->invitation_token_id);

        if (!$organisation) {
            $this->addError('invitation_token_id', 'Le jeton d’invitation est invalide ou a expiré.');
        }
    }

    #[Form\Check]
    public function checkUsernameIsAvailable(): void
    {
        if (models\User::existsBy(['username' => $this->username])) {
            $this->addError('username', 'Ce nom d’utilisateur est déjà pris, veuillez en choisir un autre.');
        }
    }
}
