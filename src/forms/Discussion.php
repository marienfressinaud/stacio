<?php

namespace App\forms;

use Minz\Form;

/**
 * @extends BaseForm<\App\models\Discussion>
 */
class Discussion extends BaseForm
{
    #[Form\Field(trim: true)]
    public string $title = '';

    #[Form\Field(trim: true)]
    public string $description = '';
}
