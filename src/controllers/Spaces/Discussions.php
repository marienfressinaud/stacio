<?php

namespace App\controllers\Spaces;

use App\auth;
use App\controllers\BaseController;
use App\forms;
use App\models;
use App\utils;
use Minz\Request;
use Minz\Response;

class Discussions extends BaseController
{
    public function index(Request $request): Response
    {
        $current_user = auth\CurrentUser::get();
        $space = models\Space::require($request);

        if (!auth\Access::isAuthorized($current_user, 'read:space', $space)) {
            return Response::notFound('not_found.phtml');
        }

        $discussions = $space->discussions();

        $pinned = [];
        $unpinned = [];
        foreach ($discussions as $discussion) {
            if ($discussion->isPinned()) {
                $pinned[] = $discussion;
            } else {
                $unpinned[] = $discussion;
            }
        }

        $pinned = utils\Sorter::sortBy($pinned, 'pinned_at', order: 'asc');
        $unpinned = utils\Sorter::sortBy($unpinned, 'created_at', order: 'desc');

        return Response::ok('spaces/discussions/index.phtml', [
            'space' => $space,
            'pinned_discussions' => $pinned,
            'unpinned_discussions' => $unpinned,
            'count_archives' => models\Discussion::countArchivesFromSpace($space),
        ]);
    }

    public function archives(Request $request): Response
    {
        $current_user = auth\CurrentUser::get();
        $space = models\Space::require($request);

        if (!auth\Access::isAuthorized($current_user, 'read:space', $space)) {
            return Response::notFound('not_found.phtml');
        }

        $discussions = $space->archives();
        $discussions = utils\Sorter::sortBy($discussions, 'created_at', order: 'desc');

        return Response::ok('spaces/discussions/archives.phtml', [
            'space' => $space,
            'discussions' => $discussions,
        ]);
    }

    public function new(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $space = models\Space::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:space.discussions', $space)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('spaces/discussions/new.phtml', [
            'space' => $space,
            'form' => new forms\Discussion(),
        ]);
    }

    public function create(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $space = models\Space::require($request);
        $discussion = new models\Discussion($space, $current_user);

        if (!auth\Access::isAuthorized($current_user, 'write:space.discussions', $space)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\Discussion(model: $discussion);
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('spaces/discussions/new.phtml', [
                'space' => $space,
                'form' => $form,
            ]);
        }

        $discussion = $form->getModel();
        $discussion->save();

        return Response::redirect('discussion', [
            'id' => $discussion->id,
        ]);
    }
}
