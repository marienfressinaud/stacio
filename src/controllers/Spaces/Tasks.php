<?php

namespace App\controllers\Spaces;

use App\auth;
use App\controllers\BaseController;
use App\forms;
use App\models;
use App\utils;
use Minz\Request;
use Minz\Response;

class Tasks extends BaseController
{
    public function index(Request $request): Response
    {
        $current_user = auth\CurrentUser::get();
        $space = models\Space::require($request);

        if (!auth\Access::isAuthorized($current_user, 'read:space', $space)) {
            return Response::notFound('not_found.phtml');
        }

        $active_tasks = $space->activeTasks();
        $active_tasks = utils\Sorter::sortBy($active_tasks, 'scheduled_at', order: 'asc');
        $done_tasks = $space->doneTasks();
        $done_tasks = utils\Sorter::sortBy($done_tasks, 'done_at', order: 'desc');

        return Response::ok('spaces/tasks/index.phtml', [
            'space' => $space,
            'active_tasks' => $active_tasks,
            'done_tasks' => $done_tasks,
        ]);
    }

    public function new(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $space = models\Space::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:space.tasks', $space)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('spaces/tasks/new.phtml', [
            'space' => $space,
            'form' => new forms\NewTask(),
        ]);
    }

    public function create(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $space = models\Space::require($request);
        $task = new models\Task($space, $current_user);

        if (!auth\Access::isAuthorized($current_user, 'write:space.tasks', $space)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\NewTask(model: $task);
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('spaces/tasks/new.phtml', [
                'space' => $space,
                'form' => $form,
            ]);
        }

        $task = $form->getModel();
        $task->insertLast();

        return Response::redirect('tasks', [
            'id' => $space->id,
        ]);
    }
}
