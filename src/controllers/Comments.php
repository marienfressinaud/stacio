<?php

namespace App\controllers;

use App\auth;
use App\forms;
use App\models;
use Minz\Request;
use Minz\Response;

class Comments extends BaseController
{
    public function create(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $comment = new models\Comment($current_user);

        $form = new forms\Comment(model: $comment);
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('comments/_new.phtml', [
                'form' => $form,
            ]);
        }

        $comment = $form->getModel();
        $reference = $comment->reference();

        if (!auth\Access::isAuthorized($current_user, 'write:space.comments', $reference->space())) {
            return Response::notFound('not_found.phtml');
        }

        $comment->save();

        return Response::found($comment->referenceUrl());
    }

    public function edit(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $comment = models\Comment::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:comment', $comment)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('comments/edit.phtml', [
            'comment' => $comment,
            'form' => new forms\Comment(model: $comment),
        ]);
    }

    public function update(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $comment = models\Comment::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:comment', $comment)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\Comment(model: $comment);
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('comments/edit.phtml', [
                'comment' => $comment,
                'form' => $form,
            ]);
        }

        $comment = $form->getModel();
        $reference = $comment->reference();

        if (!auth\Access::isAuthorized($current_user, 'write:space.comments', $reference->space())) {
            return Response::notFound('not_found.phtml');
        }

        $comment->save();

        return Response::ok('comments/_comment.phtml', [
            'comment' => $comment,
        ]);
    }

    public function delete(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $comment = models\Comment::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:comment', $comment)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\BaseForm();
        $form->handleRequest($request);

        if ($form->validate()) {
            $comment->remove();
        }

        return Response::found($comment->referenceUrl());
    }
}
