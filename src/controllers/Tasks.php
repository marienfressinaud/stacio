<?php

namespace App\controllers;

use App\auth;
use App\forms;
use App\models;
use Minz\Request;
use Minz\Response;

class Tasks extends BaseController
{
    public function show(Request $request): Response
    {
        $current_user = auth\CurrentUser::get();
        $task = models\Task::require($request);

        if (!auth\Access::isAuthorized($current_user, 'read:task', $task)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('tasks/show.phtml', [
            'task' => $task,
            'commentForm' => forms\Comment::buildFromReference($task),
        ]);
    }

    public function edit(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $task = models\Task::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:task', $task)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('tasks/edit.phtml', [
            'task' => $task,
            'form' => new forms\Task(model: $task),
        ]);
    }

    public function update(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $task = models\Task::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:task', $task)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\Task(model: $task);
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('discussions/edit.phtml', [
                'task' => $task,
                'form' => $form,
            ]);
        }

        $task = $form->getModel();
        $task->save();

        $assignees = $form->getAssignees();
        models\TaskAssignee::set($task, $assignees);

        return Response::redirect('task', [
            'id' => $task->id,
        ]);
    }

    public function updateDone(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $task = models\Task::require($request);

        $done = $request->paramBoolean('done');
        $from = $request->param('from');

        if (!auth\Access::isAuthorized($current_user, 'write:task', $task)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\BaseForm();
        $form->handleRequest($request);

        if ($form->validate()) {
            if ($done) {
                $task->done();
            } else {
                $task->undone();
            }
        }

        if ($from && $this->isPathRedirectable($from)) {
            return Response::found($from);
        } else {
            return Response::redirect('tasks', [
                'id' => $task->space_id,
            ]);
        }
    }

    public function updatePosition(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $task = models\Task::require($request);

        $position = $request->paramInteger('position', 0);

        if (!auth\Access::isAuthorized($current_user, 'write:task', $task)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\BaseForm();
        $form->handleRequest($request);

        if ($form->validate()) {
            $task->moveAtPosition($position);
        }

        return Response::redirect('tasks', [
            'id' => $task->space_id,
        ]);
    }

    public function delete(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $task = models\Task::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:task', $task)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\BaseForm();
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::redirect('task', ['id' => $task->id]);
        }

        $task->remove();

        models\Comment::deleteBy([
            'reference_type' => $task->commentableType(),
            'reference_id' => $task->id,
        ]);

        return Response::redirect('tasks', ['id' => $task->space_id]);
    }
}
