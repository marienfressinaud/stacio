<?php

namespace App\controllers;

use App\auth;
use App\forms;
use App\models;
use Minz\Request;
use Minz\Response;

class Sessions extends BaseController
{
    public function new(Request $request): Response
    {
        $current_user = auth\CurrentUser::get();

        if ($current_user) {
            return \Minz\Response::redirect('home');
        }

        return \Minz\Response::ok('sessions/new.phtml', [
            'form' => new forms\Login(),
        ]);
    }

    public function create(Request $request): Response
    {
        $current_user = auth\CurrentUser::get();

        if ($current_user) {
            return \Minz\Response::redirect('home');
        }

        $form = new forms\Login();
        $form->handleRequest($request);

        if (!$form->validate()) {
            return \Minz\Response::badRequest('sessions/new.phtml', [
                'form' => $form,
            ]);
        }

        $user = $form->getUser();

        $token = new models\Token(1, 'month');
        $session = new models\Session($user, $token);
        $token->save();
        $session->save();

        auth\CurrentUser::setSessionToken($token);

        $response = \Minz\Response::redirect('home');
        $response->setCookie('session_token', $token->id, [
            'expires' => $token->expired_at->getTimestamp(),
            'samesite' => 'Lax',
        ]);
        return $response;
    }

    public function delete(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();

        $form = new forms\BaseForm();
        $form->handleRequest($request);

        if (!$form->validate()) {
            return \Minz\Response::redirect('home');
        }

        $session_token_id = auth\CurrentUser::sessionTokenId();
        models\Token::deleteBy(['id' => $session_token_id]);
        auth\CurrentUser::reset();

        $response = \Minz\Response::redirect('home');
        $response->removeCookie('session_token');
        return $response;
    }
}
