<?php

namespace App\controllers\Discussions;

use App\auth;
use App\controllers\BaseController;
use App\forms;
use App\models;
use Minz\Request;
use Minz\Response;

class Archives extends BaseController
{
    public function update(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $discussion = models\Discussion::require($request);

        $archive = $request->paramBoolean('archive');

        if (!auth\Access::isAuthorized($current_user, 'write:discussion', $discussion)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\BaseForm();
        $form->handleRequest($request);

        if ($form->validate()) {
            if ($archive) {
                $discussion->archived_at = \Minz\Time::now();
            } else {
                $discussion->archived_at = null;
            }

            $discussion->save();
        }

        return Response::redirect('discussion', ['id' => $discussion->id]);
    }
}
