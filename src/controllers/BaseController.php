<?php

namespace App\controllers;

use App\auth;
use App\models;
use Minz\Controller;
use Minz\Request;
use Minz\Response;

class BaseController
{
    #[Controller\ErrorHandler(auth\MissingCurrentUserError::class)]
    public function redirectOnMissingCurrentUser(Request $request): Response
    {
        return Response::redirect('login');
    }

    #[Controller\ErrorHandler(models\MissingResourceError::class)]
    public function failForResource(Request $request): Response
    {
        return Response::notFound('not_found.phtml');
    }

    public function isPathRedirectable(string $path): bool
    {
        $router = \Minz\Engine::router();

        if ($router === null) {
            return false;
        }

        try {
            $router->match('GET', $path);
            return true;
        } catch (\Minz\Errors\RouteNotFoundError $e) {
            return false;
        }
    }
}
