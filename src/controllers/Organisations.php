<?php

namespace App\controllers;

use App\auth;
use App\forms;
use App\models;
use Minz\Request;
use Minz\Response;

class Organisations extends BaseController
{
    public function index(Request $request): Response
    {
        $current_user = auth\CurrentUser::get();

        $all = $request->paramBoolean('all');

        if ($current_user && !$all) {
            $organisations = models\Organisation::listBy([
                'created_by_id' => $current_user->id,
            ], 'name ASC');
        } else {
            $organisations = models\Organisation::listAll('name ASC');
        }

        return Response::ok('organisations/index.phtml', [
            'organisations' => $organisations,
            'all' => $all,
        ]);
    }

    public function new(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();

        return Response::ok('organisations/new.phtml', [
            'form' => new forms\Organisation(),
        ]);
    }

    public function create(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $organisation = new models\Organisation($current_user);

        $form = new forms\Organisation(model: $organisation);
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('organisations/new.phtml', [
                'form' => $form,
            ]);
        }

        $organisation = $form->getModel();

        $database = \Minz\Database::get();
        $database->beginTransaction();

        $invitation_token = new models\Token(999, 'year', length: 25);
        $invitation_token->save();
        $organisation->invitation_token_id = $invitation_token->id;

        $organisation->save();

        $user_in_organisation = new models\UserInOrganisation($current_user, $organisation);
        $user_in_organisation->save();

        $default_space = new models\Space($organisation, $current_user);
        $default_space->name = 'Gare centrale';
        $default_space->description = 'Le point de rendez-vous pour se rencontrer';
        $default_space->save();

        $database->commit();

        return Response::redirect('organisation', [
            'id' => $organisation->id,
        ]);
    }

    public function edit(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $organisation = models\Organisation::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:organisation', $organisation)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('organisations/edit.phtml', [
            'organisation' => $organisation,
            'form' => new forms\Organisation(model: $organisation),
        ]);
    }

    public function update(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $organisation = models\Organisation::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:organisation', $organisation)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\Organisation(model: $organisation);
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('organisations/edit.phtml', [
                'organisation' => $organisation,
                'form' => $form,
            ]);
        }

        $organisation = $form->getModel();
        $organisation->save();

        return Response::redirect('organisation', [
            'id' => $organisation->id,
        ]);
    }

    public function deletion(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $organisation = models\Organisation::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:organisation', $organisation)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('organisations/deletion.phtml', [
            'organisation' => $organisation,
            'form' => new forms\BaseForm(),
        ]);
    }

    public function delete(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $organisation = models\Organisation::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:organisation', $organisation)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\BaseForm();
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('organisations/deletion.phtml', [
                'organisation' => $organisation,
                'form' => $form,
            ]);
        }

        $organisation->remove();

        return Response::redirect('organisations');
    }

    public function join(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();

        $invitation_token_id = $request->param('token', '');

        $organisation = models\Organisation::findByInvitationTokenId($invitation_token_id);

        if (!$organisation) {
            return Response::notFound('not_found.phtml');
        }

        if (!models\UserInOrganisation::isUserInOrganisation($current_user, $organisation)) {
            $user_in_organisation = new models\UserInOrganisation($current_user, $organisation);
            $user_in_organisation->save();
        }

        return Response::redirect('spaces', ['id' => $organisation->id]);
    }

    public function show(Request $request): Response
    {
        $organisation_id = $request->paramInteger('id', 0);
        return Response::redirect('spaces', ['id' => $organisation_id]);
    }
}
