<?php

namespace App\controllers;

use App\auth;
use App\forms;
use App\models;
use Minz\Request;
use Minz\Response;

class Spaces extends BaseController
{
    public function show(Request $request): Response
    {
        $current_user = auth\CurrentUser::get();
        $space = models\Space::require($request);

        if (!auth\Access::isAuthorized($current_user, 'read:space', $space)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('spaces/show.phtml', [
            'space' => $space,
        ]);
    }

    public function edit(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $space = models\Space::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:space', $space)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('spaces/edit.phtml', [
            'space' => $space,
            'form' => new forms\Space(model: $space),
        ]);
    }

    public function update(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $space = models\Space::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:space', $space)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\Space(model: $space);
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('spaces/edit.phtml', [
                'space' => $space,
                'form' => $form,
            ]);
        }

        $space = $form->getModel();
        $space->save();

        return Response::redirect('space', [
            'id' => $space->id,
        ]);
    }

    public function deletion(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $space = models\Space::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:space', $space)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('spaces/deletion.phtml', [
            'space' => $space,
            'form' => new forms\BaseForm(),
        ]);
    }

    public function delete(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $space = models\Space::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:space', $space)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\BaseForm();
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('spaces/deletion.phtml', [
                'space' => $space,
                'form' => $form,
            ]);
        }

        $space->remove();

        return Response::redirect('organisation', ['id' => $space->organisation_id]);
    }
}
