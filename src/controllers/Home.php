<?php

namespace App\controllers;

use App\auth;
use Minz\Request;
use Minz\Response;

class Home extends BaseController
{
    public function show(Request $request): Response
    {
        $current_user = auth\CurrentUser::get();

        if ($current_user) {
            return Response::redirect('organisations');
        } else {
            return Response::ok('home/show.phtml');
        }
    }
}
