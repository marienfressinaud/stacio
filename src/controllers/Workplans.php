<?php

namespace App\controllers;

use App\auth;
use App\utils;
use Minz\Request;
use Minz\Response;

class Workplans extends BaseController
{
    public function show(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();

        $tasks = $current_user->tasks();
        $tasks = utils\Sorter::sortBy($tasks, 'position', order: 'asc');

        $grouped_tasks = [];
        foreach ($tasks as $task) {
            $scheduled_at = $task->scheduled_at;

            if ($scheduled_at) {
                $scheduled_at = $scheduled_at->format('Y-m-d');
            } else {
                $scheduled_at = '';
            }

            $grouped_tasks[$scheduled_at][] = $task;
        }

        uksort($grouped_tasks, function ($scheduled_at1, $scheduled_at2): int {
            if (!$scheduled_at1) {
                return 1;
            }

            if (!$scheduled_at2) {
                return -1;
            }

            return strcmp($scheduled_at1, $scheduled_at2);
        });

        return Response::ok('workplans/show.phtml', [
            'tasks_by_scheduled_at' => $grouped_tasks,
        ]);
    }
}
