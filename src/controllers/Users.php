<?php

namespace App\controllers;

use App\auth;
use App\forms;
use App\models;
use Minz\Request;
use Minz\Response;

class Users extends BaseController
{
    public function new(Request $request): Response
    {
        $invitation_token_id = $request->param('i', '');

        if (auth\CurrentUser::get()) {
            return Response::redirect('join organisation', [
                'token' => $invitation_token_id,
            ]);
        }

        return Response::ok('users/new.phtml', [
            'form' => new forms\Registration([
                'invitation_token_id' => $invitation_token_id,
            ]),
        ]);
    }

    public function create(Request $request): Response
    {
        $invitation_token_id = $request->param('i', '');

        if (auth\CurrentUser::get()) {
            return Response::redirect('join organisation', [
                'token' => $invitation_token_id,
            ]);
        }

        $user = new models\User();
        $form = new forms\Registration([
            'invitation_token_id' => $invitation_token_id,
        ], model: $user);

        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('users/new.phtml', [
                'form' => $form,
            ]);
        }

        $user = $form->getModel();
        $user->save();

        $token = new models\Token(1, 'month');
        $session = new models\Session($user, $token);
        $token->save();
        $session->save();

        auth\CurrentUser::setSessionToken($token);

        $response = \Minz\Response::redirect('join organisation', [
            'token' => $invitation_token_id,
        ]);
        $response->setCookie('session_token', $token->id, [
            'expires' => $token->expired_at->getTimestamp(),
            'samesite' => 'Lax',
        ]);
        return $response;
    }
}
