<?php

namespace App\controllers;

use App\auth;
use App\forms;
use App\models;
use Minz\Request;
use Minz\Response;

class Discussions extends BaseController
{
    public function show(Request $request): Response
    {
        $current_user = auth\CurrentUser::get();
        $discussion = models\Discussion::require($request);

        if (!auth\Access::isAuthorized($current_user, 'read:discussion', $discussion)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('discussions/show.phtml', [
            'discussion' => $discussion,
            'commentForm' => forms\Comment::buildFromReference($discussion),
        ]);
    }

    public function edit(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $discussion = models\Discussion::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:discussion', $discussion)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('discussions/edit.phtml', [
            'discussion' => $discussion,
            'form' => new forms\Discussion(model: $discussion),
        ]);
    }

    public function update(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $discussion = models\Discussion::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:discussion', $discussion)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\Discussion(model: $discussion);
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('discussions/edit.phtml', [
                'discussion' => $discussion,
                'form' => $form,
            ]);
        }

        $discussion = $form->getModel();
        $discussion->save();

        return Response::redirect('discussion', [
            'id' => $discussion->id,
        ]);
    }

    public function deletion(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $discussion = models\Discussion::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:discussion', $discussion)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('discussions/deletion.phtml', [
            'discussion' => $discussion,
            'form' => new forms\BaseForm(),
        ]);
    }

    public function delete(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $discussion = models\Discussion::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:discussion', $discussion)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\BaseForm();
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('discussions/deletion.phtml', [
                'discussion' => $discussion,
                'form' => $form,
            ]);
        }

        $discussion->remove();

        models\Comment::deleteBy([
            'reference_type' => $discussion->commentableType(),
            'reference_id' => $discussion->id,
        ]);

        return Response::redirect('discussions', ['id' => $discussion->space_id]);
    }
}
