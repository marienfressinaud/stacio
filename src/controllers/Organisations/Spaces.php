<?php

namespace App\controllers\Organisations;

use App\auth;
use App\controllers\BaseController;
use App\forms;
use App\models;
use Minz\Request;
use Minz\Response;

class Spaces extends BaseController
{
    public function index(Request $request): Response
    {
        $current_user = auth\CurrentUser::get();
        $organisation = models\Organisation::require($request);

        if ($current_user && models\UserInOrganisation::isUserInOrganisation($current_user, $organisation)) {
            $spaces = $organisation->spaces(confidential: true);
        } else {
            $spaces = $organisation->spaces(confidential: false);
        }

        return Response::ok('organisations/spaces/index.phtml', [
            'organisation' => $organisation,
            'spaces' => $spaces,
        ]);
    }

    public function new(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $organisation = models\Organisation::require($request);

        if (!auth\Access::isAuthorized($current_user, 'write:organisation.spaces', $organisation)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('organisations/spaces/new.phtml', [
            'organisation' => $organisation,
            'form' => new forms\Space(),
        ]);
    }

    public function create(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $organisation = models\Organisation::require($request);
        $space = new models\Space($organisation, $current_user);

        if (!auth\Access::isAuthorized($current_user, 'write:organisation.spaces', $organisation)) {
            return Response::notFound('not_found.phtml');
        }

        $form = new forms\Space(model: $space);
        $form->handleRequest($request);

        if (!$form->validate()) {
            return Response::badRequest('organisations/spaces/new.phtml', [
                'organisation' => $organisation,
                'form' => $form,
            ]);
        }

        $space = $form->getModel();
        $space->save();

        return Response::redirect('space', [
            'id' => $space->id,
        ]);
    }
}
