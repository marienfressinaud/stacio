<?php

namespace App\controllers\Organisations;

use App\auth;
use App\controllers\BaseController;
use App\models;
use Minz\Request;
use Minz\Response;

class Users extends BaseController
{
    public function index(Request $request): Response
    {
        $current_user = auth\CurrentUser::require();
        $organisation = models\Organisation::require($request);

        if (!auth\Access::isAuthorized($current_user, 'read:organisation.users', $organisation)) {
            return Response::notFound('not_found.phtml');
        }

        return Response::ok('organisations/users/index.phtml', [
            'organisation' => $organisation,
            'users' => $organisation->users(),
        ]);
    }
}
